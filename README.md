# External Experiment - Create CI for a standalone Bazel Project on Gitlab.com

This is a repo for experiments about gitlab CI for bazel projects

For py_binary, py_binary, py_test targets, we can simply build/run them in bazel image.

For py_image, container_image, .... targets which would implictly use docker, we should first build them in bazel image and get a tar file. Then we can load the tar file using docker so that we can run the container in a docker-in-docker service.

Below are some good articles to read, which are about improving bazel performance on Gitlab CI:
- https://about.gitlab.com/blog/2020/09/01/using-bazel-to-speed-up-gitlab-ci-builds/
- https://filipnikolovski.com/posts/bazel-performance-in-a-ci-environment/
